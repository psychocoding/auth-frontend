import React, { Component } from "react";
import config from '../config'
import "./Home.css";

export default class Home extends Component {
  componentWillMount() {
    let search = window.location.search;
    let params = new URLSearchParams(search);
    let redirectUrl = params.get('redirectUrl');
    if (config.VALID_REDIRECT_URL.indexOf(redirectUrl) < 0) {
      this.props.history.push("/invalid-redirect");
    }
    this.setState({
      redirectUrl
    })
  }

  render() {
    return (
      <div className="Home">
        <div className="lander">
          <h1>Please wait while we setting you up...</h1>
        </div>
      </div>
    );
  }
}