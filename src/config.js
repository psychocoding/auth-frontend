export default {
  VALID_REDIRECT_URL: [
    "http://www.google.com"
  ],
  cognito: {
    REGION: "ap-southeast-1",
    USER_POOL_ID: "ap-southeast-1_4nzp3Vm1k",
    APP_CLIENT_ID: "3m5bu7ghvfcan25sbtbd4u0tkb",
    IDENTITY_POOL_ID: "ap-southeast-1:70a516cf-88ab-4e41-a1b7-97539a470f6f"
  }
};